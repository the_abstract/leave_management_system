package com.hexaware.lms.model;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;



public class LeaveDetailsTest {

	
	@Test
	public void setLeaveIdTest() {
		LeaveDetails leaveDetails = new LeaveDetails(2900, LocalDate.of(2018, 12, 06), LocalDate.of(2018, 12, 16));
		leaveDetails.setLeaveId(1000);
		Assert.assertEquals(1000,leaveDetails.getLeaveId());
	}
	
//	@Test
//	public void setLocalStartDateTest() {
//		LeaveDetails leaveDetails = new LeaveDetails(2900, LocalDate.of(2018, 12, 06), LocalDate.of(2018, 12, 16));
//		leaveDetails.setStartDate(LocalDate.of(2018, 12, 06));
//		Assert.assertEquals(2900, leaveDetails.getStartDate());
//	}
	
	@Test
	public void toStringTest() {
		LeaveDetails leaveDetails = new LeaveDetails(1000, LocalDate.of(2019, 12, 02), LocalDate.of(2019, 12, 12));
		leaveDetails.setReasonOfLeave("sick leave");
		String expected = "LeaveDetails [leaveId=" + leaveDetails.getLeaveId() + ", startDate=" + leaveDetails.getStartDate() + ", endDate=" + leaveDetails.getEndDate()
				+ ", reasonOfLeave=" + leaveDetails.getReasonOfLeave() + ", typeStatus=" + leaveDetails.typeStatus + "]";
		Assert.assertEquals(expected, leaveDetails.toString());
	}
	
	@Test
	public void equalsAndHashCodeTest() {
		LeaveDetails leaveDetails = new LeaveDetails(1000, LocalDate.of(2019, 02, 02), LocalDate.of(2019, 02, 12));
		LeaveDetails leaveDetails2 = new LeaveDetails(1000, LocalDate.of(2019, 02, 02), LocalDate.of(2019, 02, 12));
		Assert.assertEquals(leaveDetails.equals(leaveDetails2), leaveDetails2.equals(leaveDetails));
		Assert.assertEquals(leaveDetails.hashCode(), leaveDetails2.hashCode());
	}
	
	@Test
	public void TypeStatusTest() {
		LeaveDetails leaveDetails = new LeaveDetails(1000, LocalDate.of(2019, 02, 02), LocalDate.of(2019, 02, 12));
		leaveDetails.setTypeStatus(LeaveStatus.PENDING);
		Assert.assertEquals(LeaveStatus.PENDING, leaveDetails.getTypeStatus());
	}
	
	@Test
	public void StartDateTest() {
		LeaveDetails leaveDetails = new LeaveDetails(1000, LocalDate.of(2019, 02,02), LocalDate.of(2019, 02, 12));
		//leaveDetails.setStartDate(LocalDate.of(2019, 02, 02));
		
		Assert.assertEquals(LocalDate.of(2019, 02, 02), leaveDetails.getStartDate());

}
	
	@Test
	public void EndDateTest() {
		LeaveDetails leaveDetails = new LeaveDetails(1000, LocalDate.of(2019, 02,02), LocalDate.of(2019, 02, 12));
		//leaveDetails.setEndDate(LocalDate.of(2019, 02, 12));
		Assert.assertEquals(LocalDate.of(2019, 02, 02), leaveDetails.getEndDate());
	}	

	@Test
	public void employeeManagerTest() {
	LeaveDetails leaveDetails = new LeaveDetails(1000, LocalDate.of(2019, 3, 12), LocalDate.of(2019, 3, 16));
	Employee employee = new Employee(2900, "sameer", LocalDate.of(2019, 12, 12), "sameer@gmail.com", 5);
	leaveDetails.setManager(employee);
	Assert.assertEquals(2900, leaveDetails.getManager().getEmployeeId());
}
	
}
