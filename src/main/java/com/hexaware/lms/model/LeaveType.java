package com.hexaware.lms.model;

public enum LeaveType {
	
	CASUAL,
	SICK,
	OPTIONAL;

}
