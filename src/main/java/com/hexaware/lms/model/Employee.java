package com.hexaware.lms.model;

import java.time.LocalDate;

public class Employee {
	
	private long employeeId;
	private String name;
	private LocalDate dateOfJoining;
	private String email;
	private Employee manager;
	private int leaveBalance;
	
	public Employee(long id, String name, LocalDate dateOfJoining, String email, int leaveBalance) {
		this.employeeId = id;
		this.name = name;
		this.dateOfJoining = dateOfJoining;
		this.email = email;
		this.leaveBalance = leaveBalance;
	}

	

	public long getEmployeeId() {
		return employeeId;
	}



	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(LocalDate dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

	public Employee getManager() {
		return manager;
	}

	public void setLeaveBalance(int leaveBalance) {
		this.leaveBalance = leaveBalance;
	}

	public int getLeaveBalance() {
		// TODO Auto-generated method stub
		return this.leaveBalance;
	}


	public void setManagerId(Employee manager) {
		this.manager = manager;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateOfJoining == null) ? 0 : dateOfJoining.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (int) (employeeId ^ (employeeId >>> 32));
		result = prime * result + ((manager == null) ? 0 : manager.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (dateOfJoining == null) {
			if (other.dateOfJoining != null)
				return false;
		} else if (!dateOfJoining.equals(other.dateOfJoining))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (employeeId != other.employeeId)
			return false;
		if (manager == null) {
			if (other.manager != null)
				return false;
		} else if (!manager.equals(other.manager))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", name=" + name + ", dateOfJoining=" + dateOfJoining + ", email="
				+ email + ", manager=" + manager + "]";
	}


}
