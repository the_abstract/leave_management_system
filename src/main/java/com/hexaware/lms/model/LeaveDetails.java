package com.hexaware.lms.model;

import java.time.LocalDate;

public class LeaveDetails {
	
	private int leaveId;
	private LocalDate startDate;
	private String comments;
	private LocalDate endDate;
	private String reasonOfLeave;
	LeaveStatus typeStatus;
	private Employee manager;
	
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public LeaveStatus getTypeStatus() {
		return typeStatus;
	}
	public void setTypeStatus(LeaveStatus typeStatus) {
		this.typeStatus = typeStatus;
	}

	public Employee getManager() {
		return manager;
	}
	public void setManager(Employee manager) {
		this.manager = manager;
	}
	public LeaveDetails(int leaveId, LocalDate startDate, LocalDate endDate) {
		super();
		this.leaveId = leaveId;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	@Override
	public String toString() {
		return "LeaveDetails [leaveId=" + leaveId + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", reasonOfLeave=" + reasonOfLeave + ", typeStatus=" + typeStatus + "]";
	}
	public String getReasonOfLeave() {
		return reasonOfLeave;
	}
	public void setReasonOfLeave(String reasonOfLeave) {
		this.reasonOfLeave = reasonOfLeave;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + leaveId;
		result = prime * result + ((reasonOfLeave == null) ? 0 : reasonOfLeave.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LeaveDetails other = (LeaveDetails) obj;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (leaveId != other.leaveId)
			return false;
		if (reasonOfLeave == null) {
			if (other.reasonOfLeave != null)
				return false;
		} else if (!reasonOfLeave.equals(other.reasonOfLeave))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}
	public int getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}
	public LocalDate getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	
	
}
