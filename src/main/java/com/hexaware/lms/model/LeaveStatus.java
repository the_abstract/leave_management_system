package com.hexaware.lms.model;

public enum LeaveStatus {

	PENDING,
	APPROVED, 
	REJECTED;

}
