package com.hexaware.lms.client;

import java.time.LocalDate;
import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.model.LeaveStatus;
import com.hexaware.lms.service.LeaveDetailsService;
import com.hexaware.lms.service.LeaveDetailsServiceImpl;

public class LeaveDetailsClient {
	
	public static void main(String[] args) {
		
		LeaveDetails leave = new LeaveDetails(1000, LocalDate.of(2019, 03, 20), LocalDate.of(2019, 03, 24));
		 
		
		LeaveDetailsService leaveDetails = new LeaveDetailsServiceImpl();
		
		leaveDetails.applyForLeave(1000, leave);
		
		leaveDetails.approveOrDeny(1000, 1090, "ghg", LeaveStatus.PENDING);
	}
	
}
