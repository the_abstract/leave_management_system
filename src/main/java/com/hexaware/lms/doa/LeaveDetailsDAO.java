package com.hexaware.lms.doa;

import java.util.Set;

import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.model.LeaveStatus;

public interface LeaveDetailsDAO {
	
	LeaveDetails save(long employeeId, LeaveDetails leave);
	
	LeaveDetails findLeaveById(long leaveId);
	
	Set<LeaveDetails> findLeaveDetailsByEmployeeId(long employeeId);
	
	//LeaveDetails update(long leaveId, LeaveDetails leave, long managerId , LeaveStatus leavestatus );
	
	Employee findEmployeeById(long employeeId);

	LeaveDetails update(long leaveId, LeaveDetails leave);

}
