package com.hexaware.lms.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.Set;

import com.hexaware.lms.doa.LeaveDetailsDAO;
import com.hexaware.lms.doa.LeaveDetailsDAOImpl;
import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.model.LeaveStatus;

public class LeaveDetailsServiceImpl implements LeaveDetailsService {

	private LeaveDetailsDAO leaveDetailsDAO = new LeaveDetailsDAOImpl();

	@Override
	public LeaveDetails applyForLeave(long employeeId, LeaveDetails leave) {

		// validate employee Id
		Employee employee = this.leaveDetailsDAO.findEmployeeById(employeeId);

		if (employee == null) {
			throw new IllegalArgumentException("Employee with id does not exists " + employeeId);
		}

		// fetch the leave balance and validate the leave balance is greater than
		// applied no of days
		LocalDate startDate = leave.getStartDate();
		LocalDate endDate = leave.getEndDate();

		int numberOfDays = Period.between(startDate, endDate).getDays();
		System.out.println(employee.getLeaveBalance());
		if (numberOfDays > employee.getLeaveBalance()) {
			throw new IllegalArgumentException("Insufficient leave balance");
		}
		LeaveDetails leaveDetails = this.leaveDetailsDAO.save(employeeId, leave);
		return leaveDetails; 
	}

	@Override
	public int getLeaveBalance(long employeeId) {
		// validate employee Id
		Employee employee = this.leaveDetailsDAO.findEmployeeById(employeeId);

		if (employee == null) {
			throw new IllegalArgumentException("Employee with id does not exists " + employeeId);
		} else {
			int balance = employee.getLeaveBalance();
			System.out.println("The leave balance is: "+balance);
			return balance;
		}

	}

	@Override
	public LeaveDetails approveOrDeny(long leaveId, long managerId, String comments, LeaveStatus leaveStatus) {
		LeaveDetails leave = this.leaveDetailsDAO.findLeaveById(leaveId);
		if (leave == null) {
			throw new IllegalArgumentException("Leave with id does not exists " + leaveId);
		}
		
		Employee employee = leave.getManager();
		long employeeId = employee.getManager().getEmployeeId();
		
		if(employee.getEmployeeId() == employeeId) {
			leave.setTypeStatus(leaveStatus);
			leave.setComments("");
			this.leaveDetailsDAO.update(leaveId, leave);
		}
		
		return leave;
	}

}
