package com.hexaware.lms.service;

import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.model.LeaveStatus;

public interface LeaveDetailsService {
	
	LeaveDetails applyForLeave(long employeeId, LeaveDetails leave);
	
	//LeaveDetails approveOrDeny(long leaveId, LeaveDetails leave, String comments);

	int getLeaveBalance(long employeeId);

	LeaveDetails approveOrDeny(long leaveId, long managerId, String comments, LeaveStatus leaveStatus);
	
	
	
	
}
